#!/bin/bash
set -e

INTERNAL_INTERFACE='enp3s5'
EXTERNAL_INTERFACE='enp2s0'

INTERNAL_NETWORK='10.42.0.0/16'
EXTERNAL_NETWORK_MASK='20'

FORWARD_USERS_TABLE='FORWARD_USERS'
POSTROUTING_USERS_TABLE='POSTROUTING_USERS'

function setup() {
  echo "Setup..."

  echo "1" >/proc/sys/net/ipv4/ip_forward

  iptables -F
  iptables -F -t nat
  iptables -X -t nat
  iptables -F -t filter
  iptables -X -t filter

  iptables -P FORWARD DROP
  iptables -P INPUT DROP
  iptables -P OUTPUT ACCEPT

  iptables -A INPUT -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
  iptables -A FORWARD -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT

  iptables -N $FORWARD_USERS_TABLE
  iptables -A FORWARD -i $INTERNAL_INTERFACE -o $EXTERNAL_INTERFACE -s $INTERNAL_NETWORK ! -d $INTERNAL_NETWORK -j $FORWARD_USERS_TABLE

  iptables -t nat -N $POSTROUTING_USERS_TABLE
  iptables -t nat -A POSTROUTING -o $EXTERNAL_INTERFACE -s $INTERNAL_NETWORK ! -d $INTERNAL_NETWORK -j $POSTROUTING_USERS_TABLE

  echo "Done"
}

function add_user() {
  echo "Add user..."
  echo "User id: $1"
  echo "IP: $2"

  ip addr add dev $EXTERNAL_INTERFACE "$2/$EXTERNAL_NETWORK_MASK" || echo "Warning: IP already exists"

  FORWARD_USER_TABLE="FORWARD_USER_$1"

  iptables -N "$FORWARD_USER_TABLE"
  iptables -A "$FORWARD_USERS_TABLE" -j "$FORWARD_USER_TABLE"

  POSTROUTING_USER_TABLE="POSTROUTING_USER_$1"

  iptables -t nat -N "$POSTROUTING_USER_TABLE"
  iptables -t nat -A "$POSTROUTING_USERS_TABLE" -j "$POSTROUTING_USER_TABLE"
  iptables -t nat -A "$POSTROUTING_USER_TABLE" -m mark --mark "$1" -j SNAT --to-source "$2"

  echo "Done"
}

function del_user() {
  echo "Del user..."
  echo "User id: $1"
  echo "IP: $2"

  ip addr del dev $EXTERNAL_INTERFACE "$2/$EXTERNAL_NETWORK_MASK"

  FORWARD_USER_TABLE="FORWARD_USER_$1"

  iptables -D "$FORWARD_USERS_TABLE" -j "$FORWARD_USER_TABLE"
  iptables -F "$FORWARD_USER_TABLE"
  iptables -X "$FORWARD_USER_TABLE"

  POSTROUTING_USER_TABLE="POSTROUTING_USER_$1"

  iptables -t nat -D "$POSTROUTING_USERS_TABLE" -j "$POSTROUTING_USER_TABLE"
  iptables -t nat -F "$POSTROUTING_USER_TABLE"
  iptables -t nat -X "$POSTROUTING_USER_TABLE"

  echo "Done"
}

function add_device() {
  echo "Add device..."
  echo "User id: $1"
  echo "MAC: $2"

  FORWARD_USER_TABLE="FORWARD_USER_$1"

  iptables -A "$FORWARD_USER_TABLE" -m mac --mac-source "$2" -j MARK --set-mark "$1"
  iptables -A "$FORWARD_USER_TABLE" -m mac --mac-source "$2" -j ACCEPT

  echo "Done"
}

function del_device() {
  echo "Del device..."
  echo "User id: $1"
  echo "MAC: $2"

  FORWARD_USER_TABLE="FORWARD_USER_$1"

  iptables -D "$FORWARD_USER_TABLE" -m mac --mac-source "$2" -j MARK --set-mark "$1"
  iptables -D "$FORWARD_USER_TABLE" -m mac --mac-source "$2" -j ACCEPT

  echo "Done"
}

function check_dependencies() {
  for f in echo iptables ip; do
    command -v $f >/dev/null 2>&1 || (
      echo "Executable $f not found"
      exit 1
    )
  done
}

function help() {
  echo
  echo "Commands:"
  echo
  echo "    setup                                 Prepare iptables"
  echo
  echo "    add_user <user_id> <public_ip>        Create new user with id and public ip address"
  echo "    del_user <user_id> <public_ip>        Delete user with id and public ip address and all related devices"
  echo
  echo "    add_device <user_id> <mac_address>    Add device with mac address to user with specified id"
  echo "    del_device <user_id> <mac_address>    Delete device with mac address from user with specified id"
  echo
  echo "Examples:"
  echo
  echo "    add_user 1 153.19.208.231"
  echo "    add_device 1 B8:6B:23:31:AA:1A"
  echo "    del_device 1 B8:6B:23:31:AA:1A"
  echo "    del_user 1 153.19.208.231"

}

check_dependencies

case $1 in
setup)
  "$@"
  exit
  ;;
add_user)
  "$@"
  exit
  ;;
del_user)
  "$@"
  exit
  ;;
add_device)
  "$@"
  exit
  ;;
del_device)
  "$@"
  exit
  ;;
help)
  "$@"
  exit
  ;;
esac

echo "Bad option: use help"
exit 1
